var boxs = [];
var memorizeTime = 500;
var numBox = 9;
var sizeBox = 100;
var score=0;
var start = false;
var popSound = new sound("pop.wav");
var failSound = new sound("fail.mp3");
var passSound=new sound("pass.mp3");
var c = document.createElement('canvas');
var ctx = c.getContext("2d");
var startTime = new Date();
c.width = window.innerWidth;
c.height = window.innerHeight;
c.style.background = "#000000";
c.addEventListener("click", onDown, false);
document.body.appendChild(c);
var circleReset = {
    x: 70,
    y:  c.height - 80,
    r: 60,
}
function onDown(event) {
    var posX = c.getBoundingClientRect().left;
    var posY = c.getBoundingClientRect().top;
    x = event.pageX - posX;
    y = event.pageY - posY;
    hitbox(x, y);
    hitcirle(x, y);
}
function reset() {
    boxs.splice(0, boxs.length);
    gamePlay();
}
function sound(src) {
    this.sound = document.createElement("audio");
    this.sound.src = src;
    this.sound.setAttribute("preload", "auto");
    this.sound.setAttribute("controls", "none");
    this.sound.style.display = "none";
    document.body.appendChild(this.sound);
    this.play = function () {
        this.sound.play();
    }
    this.stop = function () {
        this.sound.pause();
    }
}
function hitcirle(x, y) {
    var dis = Math.pow(x - circleReset.x, 2) + Math.pow(y - circleReset.y, 2);
    var R = Math.pow(circleReset.r, 2);
    if (dis <= R) {
        start = true;
        reset();
    }
}
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function hitbox(x, y) {
    var vt = false;
    for (let i = 0; i < boxs.length; i++) {
        if (x >= boxs[i].x && x <= boxs[i].x + 100
            && y >= boxs[i].y && y <= boxs[i].y + 100) {
            if (i == 0) vt = true;
            else {
                failSound.play();
                reset();
            }
        }
    }
    if (vt) {
        boxs.splice(0, 1);
        popSound.play();
        if (boxs.length==0) {
            score++;
            passSound.play();
        }
    }
}
function box() {
    this.x = getRndInteger(120, c.width - 100);
    this.y = getRndInteger(0, c.height - 100);

    this.draw = () => {
        ctx.fillStyle = "#fff";
        ctx.fillRect(this.x, this.y, 100, 100);
    }
    this.drawText = (text) => {
        ctx.font = "90px Comic Sans MS";
        ctx.fillStyle = "white";
        ctx.textAlign = "center";
        ctx.fillText(text, this.x + 50, this.y + 90);
    }
}
function gamePlay() {
    ctx.clearRect(0, 0, c.width, c.height);
    ctx.font = "30px Arial"
    ctx.fillStyle = "white";
    ctx.textAlign = "center";
    ctx.fillText("Start", 70, c.height - 70);
    ctx.font = "20px Arial"
    ctx.fillText("Banana: "+ score, 70, c.height - 180);
    ctx.beginPath();
    ctx.strokeStyle = "white";
    ctx.arc(circleReset.x, circleReset.y, circleReset.r, 0, 2 * Math.PI);
    ctx.stroke();
    if (start) {
        start = false;
        startTime = new Date();
        for (let i = 0; i < numBox; i++) {
            var flag = true;
            var b;
            while (flag) {
                b = new box();
                flag = false;
                if (boxs.length == 0) break;
                for (let i = 0; i < boxs.length; i++) {
                    if (b.x <= boxs[i].x + 100
                        && b.y <= boxs[i].y + 100
                        && b.x + 100 >= boxs[i].x
                        && b.y + 100 >= boxs[i].y) {
                        flag = true;
                    }
                }
            }
            boxs.push(b);
        }
    }
    for (let i = 0; i < boxs.length; i++) {
        if (new Date() - startTime <= memorizeTime)
            boxs[i].drawText(i + 1);
        else boxs[i].draw();
    }
    requestAnimationFrame(gamePlay);
}
$(document).ready(gamePlay());
